require "octokit"

class Task

  def initialize
    @token = "8becf127514742ab4b11402d620afb13eeb0caad"
    @login = "rowen92"
    @client = Octokit::Client.new(:access_token => @token)
    Octokit.auto_paginate = true
  end

  def get_repositories
    @user_repositories = @client.repositories( user = @login )
    puts "#{@login} repositories"
    @user_repositories.each do |rep|
      puts "- #{rep.name}"
    end
  end

  def get_repositories_followers
    @client.followers.each do |fl|
      puts "#{fl.login} repositories"
      @client.repositories( user = fl.login ).each do |rep|
        puts "- #{rep.name}"
      end
    end
  end

  def maximalist
    puts "MAXIMALIST"
    @max = 0
    @client.followers.each do |fl|
      @min = @client.repositories( user = fl.login ).size
      if @min > @max
        @max = @min
        @maximalist = fl
      end
    end
    puts "#{@maximalist.login} - #{@max}"
  end
end

task = Task.new
task.get_repositories
task.get_repositories_folowers
task.maximalist
